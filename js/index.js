/*---Теоритичні питання---
1) Опишіть своїми словами, що таке метод об'єкту
Метод об'єкту - це функція, визначена як властивість об'єкта, яка може отримувати доступ до властивостей 
цього об'єкта та змінювати їх.
2) Який тип даних може мати значення властивості об'єкта?
Рядки, числа, логічні зачення, масиви, об'єкти, функції, Null i Undefined.
3) Об'єкт це посилальний тип даних. Що означає це поняття?
У  js об'єкти вважаються довідковими типами даних,оскільки змінна фактично не зберігає об'єк,
змінна фактично не зберігає сам об'єкт, а зберігає посилання або вказівки на місце пам'яті
де зберігається  об'єкт

*---Практичне завдання---*/

const user =  createNewUser();

function createNewUser() {
   const firstName = prompt('Enter your first name:');
   const lastName = prompt('Enter your last name');
   
    const newUser = {
    firstName: firstName,
    lastName: lastName,

    getLogin: function() {
        const firstLetter = 
        this.firstName.charAt(0).toLowerCase();
        return firstLetter + this.lastName.toLowerCase(); 
    }
   };
   return newUser;
}

console.log(user);
console.log(user.getLogin());
